//将图片转换成base64字符串
        public async static Task<String> convertToString(Image image)
        {
            //var storageFolder = KnownFolders.PicturesLibrary;
            ImageSource source = image.Source;
            string imagestr = (source as BitmapImage).UriSource.AbsolutePath.Substring(1);
            imagestr = imagestr.Replace("/", "\\");
            //BitmapImage bitamp = new BitmapImage();
            //bitamp = source as BitmapImage;
            var storageFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            StorageFile file = await storageFolder.GetFileAsync(imagestr);
            IRandomAccessStream filestream = await file.OpenAsync(FileAccessMode.Read);
            var reader = new DataReader(filestream.GetInputStreamAt(0));
            await reader.LoadAsync((uint)filestream.Size);
            byte[] pixels = new byte[filestream.Size];
            reader.ReadBytes(pixels);
            string strbase64 = Convert.ToBase64String(pixels);
            return strbase64;
        }